package com.mibanco.yevo.app.function.files;

import com.mibanco.yevo.app.function.constants.Constant;
import com.mibanco.yevo.app.function.entities.SolicitudeEntity;
import com.mibanco.yevo.app.function.repositories.SolicitudeRepository;
import com.mibanco.yevo.app.function.utils.UtilDate;
import com.mibanco.yevo.app.function.utils.UtilFile;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.CloudBlockBlob;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Component
public class SolicitudeFile implements Consumer<String> {

    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss");

    @Autowired
    SolicitudeRepository solicitudeRepository;

    @Value("${file.path.consolidated}")
    private String filePath;

    @Value("${file.name.consolidated}")
    private String baseFileName;

    @Value("${file.rowtitle.consolidated}")
    private String rowTitle;

    @Value("${spring.azure.storage.account.value}")
    private String storageAccountValue;

    @Value("${spring.azure.storage.container.name}")
    private String storageContainerName;

    @Override
    public void accept(String nameTrigger) {
        LocalDateTime today = LocalDateTime.now();
        LocalDateTime yesterday = LocalDateTime.now().minusDays(5);

        Date taskStartDate = Date.from(yesterday.atZone(ZoneId.systemDefault()).toInstant());
        Date taskEndDate = Date.from(today.atZone(ZoneId.systemDefault()).toInstant());

        List<SolicitudeEntity> solicitudes = solicitudeRepository.findByRangeDates(taskStartDate, taskEndDate);
        log.info("solicitudes size: {}", solicitudes.size());
        String fileName =  this.baseFileName + dateFormatter.format(LocalDateTime.now()) + ".csv";
        String fileAndNamePath = this.filePath + "/" + fileName;

        File targetFile = new File(fileAndNamePath);

        try {
            UtilFile.createDirectoryIfNotExist(this.filePath);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(targetFile.getAbsolutePath())
                    , "Cp1252"));
            writer.append(rowTitle);
            writer.newLine();
            appendSolicitudeData(writer, solicitudes);
            writer.close();

            CloudStorageAccount account = CloudStorageAccount.parse(storageAccountValue);
            CloudBlobClient client = account.createCloudBlobClient();
            CloudBlobContainer container = client.getContainerReference(storageContainerName);
            CloudBlockBlob blob = container.getBlockBlobReference(fileName);
            log.info("fileName: {} and fileAndNamePath: {}",fileName,fileAndNamePath);
            blob.uploadFromFile(fileAndNamePath);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            try {
                FileUtils.cleanDirectory(new File(this.filePath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            ex.printStackTrace();
        }
    }

    private void appendSolicitudeData(BufferedWriter writer, List<SolicitudeEntity> solicitudes) throws IOException {
        StringBuilder sb = new StringBuilder();
        for (SolicitudeEntity solicitude : solicitudes) {
            sb.setLength(0);
            sb.append(solicitude.getSolicitudeId()).append(",");
            sb.append(solicitude.getTypeDocument()).append(",");
            sb.append(solicitude.getDocumentNumber()).append(",");
            sb.append(solicitude.getName()).append(",");
            sb.append(solicitude.getPhone()).append(",");
            sb.append(solicitude.getEmail()).append(",");
            sb.append(solicitude.getCode() != null ? solicitude.getCode() : "0").append(",");
            sb.append(UtilDate.convertToLocalDateTimeFormat(solicitude.getCreationDate())).append(",");
            sb.append(Constant.ReportValue.REPORT_SOLICITUDE).append(",");
            sb.append(solicitude.isClient() ? "SI" : "NO").append(",");
            sb.append(solicitude.getDepartment()).append(",");
            sb.append(solicitude.getProvince()).append(",");
            sb.append(solicitude.getDistrict()).append(",");
            sb.append(solicitude.getAddress().replace(",",";")).append(",");
            sb.append(solicitude.getLatitude()).append(",");
            sb.append(solicitude.getLongitude()).append(",");
            sb.append(solicitude.getAmount()).append(",");
            int typeCampaign = solicitude.getTypeCampaign();
            if (typeCampaign == 1) {
                sb.append(Constant.ReportValue.CAMPAIGN_PRE_APPROVED);
            } else if (typeCampaign == 2) {
                sb.append(Constant.ReportValue.CAMPAIGN_KUSHKA);
            } else {
                sb.append("-");
            }
            writer.append(sb.toString());
            writer.newLine();
        }
    }
}
