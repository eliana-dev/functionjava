package com.mibanco.yevo.app.function.constants;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;

import java.util.regex.Pattern;

public class Constant {
    public static final ModelMapper OBJECT_MODEL_MAPPER = new ModelMapper();
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static final String USER_ADMIN = "Admin";
    public static final String STRING_SPACE = " ";

    public static final class ReportValue
    {
        public static final String REPORT_LEAD = "Registro Semilla";
        public static final String REPORT_SOLICITUDE = "Registro Yevo";
        public static final String CAMPAIGN_PRE_APPROVED = "Pre Aprobado";
        public static final String CAMPAIGN_KUSHKA = "Kushka";
    }

    public static final class StatusValue
    {
        public static final String inactive = "I";
        public static final String active = "A";
    }

    public static final class StatusDirectory {
        public static final int Creado = 5001;
        public static final int Aprobado = 5002;
        public static final int Observado = 5003;
    }

    public static final class PaymentMethod {
        public static final int Other = 1005;
    }

    public static final class RegExp
    {
        public static final String tags = "\\<[^>]*>";
    }

    public static final class Feel
    {
        public static final String like = "like";
        public static final String dislike = "dislike";
    }

    public static final class Day
    {
        public static final String today = "today";
        public static final String yesterday = "yesterday";
    }

    public static final class Screen
    {
        public static final String FIRST = "1";
        public static final String SECOND = "2";
        public static final String THIRD = "3";
    }

    public static final class Report
    {
        public static final String REPORT_SOLICITUDE = "REPORT_SOLICITUDE";
        public static final String REPORT_LEAD = "REPORT_LEAD";
    }

    public static final class TypeDocument {
        public static final  String DNI = "C";
        public static final  String RUC = "R";
        public static final  String CARNET_EXTRANJERIA = "E";
        public static final  String PASAPORTE = "P";
    }

    public static final class Survey {
        public static final int DAYS = 7;
        public static final int FIRST = 1;
        public static final int SECOND = 2;
        public static final int THIRD = 3;
        public static final String STATUS_PENDING = "Pending";
        public static final String STATUS_COMPLETED = "Completed";
    }

    public static final class PatternValues {
        public static final Pattern VALID_DNI_REGEX = Pattern.compile("^[0-9]{8,8}$");
        public static final Pattern VALID_CARACTERES_REGEX = Pattern.compile("^[a-zA-Záéíóúñü ]{1,100}$");
        public static final  Pattern VALID_RUC_REGEX = Pattern.compile("^[0-9]{11,11}$");
        public static final  Pattern VALID_CARNET_EXTRANJERIA_REGEX = Pattern.compile("^[0-9]{12,12}$");
        public static final  Pattern VALID_PASAPORTE_REGEX = Pattern.compile("^[0-9]{12,12}$");
    }

    public static final class Credit
    {
        public static final String TYPE_LEAD = "Lead";
        public static final String TYPE_SOLICITUDE = "Solicitude";
        public static final int AMOUNT_MINIMUM_CLIENT = 300;
        public static final int AMOUNT_MINIMUM_NO_CLIENT = 100;
        public static final int AMOUNT_MAXIMUM_NO_CLIENT = 1000;
        public static final int ROUND_LIMIT_LEAD = 49;
        public static final int TYPE_PRE_APPROVED_INVALID = 0;
    }

    public static final class Verify
    {
        public static final String STATUS_PENDIND = "Pending";
        public static final String STATUS_APPROVED = "Approved";
        public static final String STATUS_MAX_ATTEMPTS_REACHED = "MaxAttemptsReached";
        public static final String STATUS_EXPIRED = "Expired";
        public static final String CHANNEL_SMS = "sms";
        public static final String CHANNEL_EMAIL = "email";
        public static final int MAX_MINUTES = 10;
        public static final int MAX_ATTEMPTS = 3;
        public static final int MAX_RESEND_ATTEMPTS = 3;
    }
}
