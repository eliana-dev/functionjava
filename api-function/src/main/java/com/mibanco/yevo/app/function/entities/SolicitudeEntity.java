package com.mibanco.yevo.app.function.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "Solicitude", schema = "seg")
public class SolicitudeEntity extends BaseEntity implements Serializable {

    private static final long serialVersionUID = -6688048221569129599L;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="SolicitudeId")
    private int solicitudeId;
    @Column(name="UserId")
    private int userId;
    @Column(name="Code")
    private String code;
    @Column(name="Name")
    private String name;
    @Column(name="Phone")
    private String phone;
    @Column(name="TypeDocument")
    private String typeDocument;
    @Column(name="DocumentNumber")
    private String documentNumber;
    @Column(name="Email")
    private String email;
    @Column(name="CampaniaEndDate")
    private Date campaniaEndDate;
    @Column(name="Ubigeo")
    private String ubigeo;
    @Column(name="Department")
    private String department;
    @Column(name="Province")
    private String province;
    @Column(name="District")
    private String district;
    @Column(name="Address")
    private String address;
    @Column(name="IsClient")
    private boolean client;
    @Column(name="Latitude")
    private String latitude;
    @Column(name="Longitude")
    private String longitude;
    @Column(name="Amount")
    private Integer amount;
    @Column(name="IsCompleted")
    private boolean completed;
    @Column(name="TypeCampaign")
    private int typeCampaign;
}
