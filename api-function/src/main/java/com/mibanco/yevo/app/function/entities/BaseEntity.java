package com.mibanco.yevo.app.function.entities;

import com.mibanco.yevo.app.function.utils.UtilDate;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity {
    @Column(name="FlagStatus")
    private String flagStatus;
    @Column(name="CreationUser")
    private String creationUser;
    @Column(name="CreationDate")
    private Date creationDate;
    @Column(name="ModificationUser")
    private String modificationUser;
    @Column(name="ModificationDate")
    private Date modificationDate;

    @PrePersist
    protected void prePersist() {
        if (this.creationDate == null) creationDate = UtilDate.getDateNow();
        if (this.flagStatus == null) flagStatus = "A";
        if (this.creationUser == null) creationUser = "Admin";
    }
}
