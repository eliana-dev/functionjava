package com.mibanco.yevo.app.function;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class ApiFunctionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiFunctionApplication.class, args);
		log.info("Application apiFunction is up and running!");
	}
}
