package com.mibanco.yevo.app.function.utils;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class UtilDate {
    private UtilDate() {
        throw new IllegalStateException("UtilDate class is an utility");
    }

    private static ZoneOffset timeZonePeru= ZoneOffset.ofHours(-5);
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/uuuu HH:mm:ss");
    private static DateTimeFormatter formatter_ddmmyyyy = DateTimeFormatter.ofPattern("d/MM/uuuu");

    public static Date getDateNow() {
        Instant instant = LocalDateTime.now().toInstant(ZoneOffset.UTC);
        return Date.from(instant);
    }

    public static LocalDateTime now() {
        return LocalDateTime.now(timeZonePeru);
    }

    public static Date nowDate() {
        return Timestamp.valueOf(now());
    }

    public static LocalDate convertToLocalDate(Date dateToConvert) {
        return LocalDate.ofInstant(dateToConvert.toInstant(), timeZonePeru);
    }

    public static LocalDateTime convertToLocalDateTime(Date dateToConvert) {
        return LocalDateTime.ofInstant(dateToConvert.toInstant(), timeZonePeru);
    }

    public static String convertToLocalDateTimeFormat(Date dateToConvert) {
        return LocalDateTime.ofInstant(dateToConvert.toInstant(), timeZonePeru).format(formatter);
    }

    public static String convertToLocalDateTimeFormat_ddmmyyyy(Date dateToConvert) {
        return LocalDateTime.ofInstant(dateToConvert.toInstant(), timeZonePeru).format(formatter_ddmmyyyy);
    }
}
