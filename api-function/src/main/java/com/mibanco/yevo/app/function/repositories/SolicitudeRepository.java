package com.mibanco.yevo.app.function.repositories;

import com.mibanco.yevo.app.function.entities.SolicitudeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SolicitudeRepository extends JpaRepository<SolicitudeEntity, Integer> {
    @Query(value = "SELECT * FROM seg.Solicitude WHERE CreationDate >= :taskStart AND CreationDate <= :taskEnd " +
            "AND FlagStatus = 'A' " +
            "AND IsCompleted = 1", nativeQuery = true)
    List<SolicitudeEntity> findByRangeDates(@Param("taskStart") Date taskStart, @Param("taskEnd") Date taskEnd);
}
