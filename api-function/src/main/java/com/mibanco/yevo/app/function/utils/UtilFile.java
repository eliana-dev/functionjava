package com.mibanco.yevo.app.function.utils;

import java.io.File;

public class UtilFile {
    public static void createDirectoryIfNotExist(String filePath) {
        File directory = new File(filePath);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }
}
