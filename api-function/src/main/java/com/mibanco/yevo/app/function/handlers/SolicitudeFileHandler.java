package com.mibanco.yevo.app.function.handlers;

import com.microsoft.azure.functions.ExecutionContext;
import com.microsoft.azure.functions.annotation.FunctionName;
import com.microsoft.azure.functions.annotation.TimerTrigger;
import org.springframework.cloud.function.adapter.azure.FunctionInvoker;

public class SolicitudeFileHandler extends FunctionInvoker<String, Void> {

    @FunctionName("solicitudeFile")
    public void execute(
            @TimerTrigger(name = "solicitudeFileTrigger", schedule = "0 */1 * * * *") String timerInfo,
            ExecutionContext context) {
        context.getLogger().info("Timer is triggered -> solicitudeFileTrigger");
        handleRequest("solicitudeFileTrigger",context);
    }
}
